extends KinematicBody2D

onready var sword_name = "sword"
onready var swing_sounds = [
	preload("res://audio/swing.wav"),
	preload("res://audio/swing2.wav"),
	preload("res://audio/swing3.wav")
]

onready var laser_sounds = [
	preload("res://audio/laser1.wav"),
	preload("res://audio/laser2.wav"),
	preload("res://audio/laser3.wav")
]

onready var ninja_sounds = [
	preload("res://audio/ninja1.wav"),
	preload("res://audio/ninja2.wav"),
	preload("res://audio/ninja3.wav")
]

func _process(delta):
	move_and_collide(get_global_mouse_position() - position)
	
	if not $Swing.playing:
		if $Steel.linear_velocity.length() > 3000:
			if sword_name == "sword":
				$Swing.stream = swing_sounds[randi() % 3]
			elif sword_name == "laser":
				$Swing.stream = laser_sounds[randi() % 3]
			elif sword_name == "katana":
				$Swing.stream = ninja_sounds[randi() % 3]
			$Swing.play()
		
func _input(event):
	if event.is_action_pressed("sword"):
		sword_name = "sword"
		$Steel/Sword.show()
		$Steel/Laser.hide()
		$Steel/Katana.hide()
		$Steel.gravity_scale = -50
	elif event.is_action_pressed("laser"):
		sword_name = "laser"
		$Steel/Sword.hide()
		$Steel/Laser.show()
		$Steel/Katana.hide()
		$Steel.gravity_scale = -50
	elif event.is_action_pressed("katana"):
		sword_name = "katana"
		$Steel/Sword.hide()
		$Steel/Laser.hide()
		$Steel/Katana.show()
		$Steel.gravity_scale = 50
