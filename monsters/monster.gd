extends RigidBody2D

onready var lifetime = 0
onready var SPEED = 600
onready var factor = SPEED

onready var hit_sounds = [
	preload("res://audio/interface1.wav"),
	preload("res://audio/interface2.wav"),
	preload("res://audio/interface3.wav")
]

func _process(delta):
	lifetime += delta
	mass = ceil(lifetime)
	
	factor = SPEED * delta * ceil(lifetime) * mass
	
	if position.y > 820:
		apply_central_impulse(Vector2.LEFT * factor + Vector2.UP * factor)
	else:
		apply_central_impulse(Vector2.LEFT * factor + Vector2.DOWN * factor)
	
	if position.y <= -1000 or position.y >= 1080 or position.x <= -300 or position.x >= 1200:
		get_parent().monster_counter += 1
		get_parent().update_counter()
		queue_free()
	
	if lifetime > 3:
		add_torque(factor*-50)
		#apply_torque_impulse(factor)


func _on_SpikyLandMonster_body_shape_entered(body_id, body, body_shape, local_shape):
	if body.name == "Steel":
		if not $HitSound.playing:
			$HitSound.stream = hit_sounds[randi() % 3]
			$HitSound.play()
