extends KinematicBody2D

onready var cooldown = false
onready var SPEED = 200
enum STATES {STAND, WALK, SLIDE, JUMP, HURT, DIE}
onready var state = STATES.WALK
onready var jump_finished = true
onready var already_dead = false
signal hit

onready var hit_sounds = [
	preload("res://audio/uah.wav"),
	preload("res://audio/ah.wav"),
	preload("res://audio/hust.wav")
]

func _process(delta):
	if state == STATES.STAND:
		$AnimatedSprite.play("stand")
	elif state == STATES.WALK:
		$AnimatedSprite.play("walk")
	elif state == STATES.SLIDE:
		$AnimatedSprite.play("slide")
		move_and_collide(Vector2.RIGHT * SPEED * delta)
	elif state == STATES.HURT:
		$AnimatedSprite.play("hurt")
	elif state == STATES.DIE:
		if not already_dead:
			die()
	elif state == STATES.JUMP:
		if not jump_finished:
			$AnimatedSprite.play("jump")
			move_and_collide(Vector2.UP * SPEED * delta)
		else:
			move_and_collide(Vector2.DOWN * SPEED * delta)
			if position.y >= 805:
				state = STATES.WALK
	
	if state != STATES.JUMP and position.y < 805:
		move_and_collide(Vector2.DOWN * SPEED * delta)

func die():
	if not $DeathSound.playing:
		$DeathSound.play()
	$AnimatedSprite.play("die")
	set_collision_layer_bit(1, false)
	set_collision_mask_bit(1, false)
	already_dead = true

func _input(event):
	if event.is_action_pressed("jump"):
		if state == STATES.WALK:
			state = STATES.JUMP
			jump_finished = false

func _on_AnimatedSprite_animation_finished():
	if $AnimatedSprite.animation == "jump":
		jump_finished = true
	if $AnimatedSprite.animation == "hurt":
		state = STATES.WALK

func _on_Area2D_body_entered(body):
	if not already_dead:
		if body is RigidBody2D:
			if not cooldown:
				cooldown = true
				$Timer.start()
				emit_signal("hit")
				$HitSound.stream = hit_sounds[randi() % 3]
				$HitSound.play()
				body.apply_central_impulse(100*(body.position - self.position))

func _on_Timer_timeout():
	cooldown = false
