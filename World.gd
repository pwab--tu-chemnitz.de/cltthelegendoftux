extends Node2D

onready var max_monster = 35
onready var pause = false
onready var game_over = false
onready var hearts = 5
onready var monsters = 0
onready var monster_counter = 0
onready var monster_speed_factor = 1
onready var monsterscript = preload("res://monsters/monster.gd")
onready var spiky_land_monster = preload("res://monsters/spiky_land_monster/SpikyLandMonster.tscn")

func _ready():
	pause()
	$BackgroundMusic.play()

func _input(event):
	if event.is_action_pressed("menu"):
		pause()
	elif event.is_action_pressed("fullscreen"):
		OS.window_fullscreen = !OS.window_fullscreen
	elif event.is_action_pressed("sound"):
		for i in range(AudioServer.bus_count):
			AudioServer.set_bus_mute(i, !AudioServer.is_bus_mute(i))

func _notification(what):
	if what == MainLoop.NOTIFICATION_WM_FOCUS_IN:
		unpause()
	elif what == MainLoop.NOTIFICATION_WM_FOCUS_OUT:
		pause()

func pause():
	$PauseLayer.show()
	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
	get_tree().paused = true

func unpause():
	$PauseLayer.hide()
	Input.set_mouse_mode(Input.MOUSE_MODE_HIDDEN)
	get_tree().paused = false

func update_counter():
	$Counter.text = str(monster_counter)
	
	if monsters == 1:
		$MonsterTimer.wait_time = 3
	elif monsters > 10:
		$MonsterTimer.wait_time = 1
		monster_speed_factor = 2
	elif monsters > 20:
		$MonsterTimer.wait_time = 0.5
		monster_speed_factor = 4
	elif monsters > 25:
		$MonsterTimer.wait_time = 0.25
		monster_speed_factor = 16
	elif monsters > 30:
		$MonsterTimer.wait_time = 0.1
		monster_speed_factor = 32
	
	if monsters >= max_monster and monsters == monster_counter:
		on_game_over(true)

func _on_MonsterTimer_timeout():
	var monster = spiky_land_monster.instance()
	monster.name = "monster"
	monster.get_node("AnimatedSprite").set_animation(str(randi()%2))
	monster.position = Vector2(1000,835)
	monster.set_script(monsterscript)
	add_child(monster)
	monster.SPEED *= monster_speed_factor
	monsters += 1
	if monsters == max_monster:
		$MonsterTimer.stop()

func _on_Retry_pressed():
	get_tree().reload_current_scene()

func _on_Tux_hit():
	hearts -= 1
	if not game_over:
		$Hearts.get_node(str(hearts+1)).set_modulate(Color.black)
		$Tux.state = $Tux.STATES.HURT
		if hearts == 0:
			on_game_over(false)

func on_game_over(won):
	game_over = true
	$Tux.get_node("AnimatedSprite").stop()
	$MonsterTimer.stop()
	$Background.scroll_speed = 0
	$Retry.show()
	$BackgroundMusic.stop()
	if won:
		$WinMusic.play()
		$Tux.state = $Tux.STATES.SLIDE
		$Retry.get_node("Won").show()
	else:
		$Tux.state = $Tux.STATES.DIE
		$Retry.get_node("Lost").show()
