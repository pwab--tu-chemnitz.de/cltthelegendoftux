extends ParallaxBackground

onready var scroll_speed = 2

func _process(delta):
	offset.x -= scroll_speed
